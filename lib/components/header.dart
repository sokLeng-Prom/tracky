// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class HeaderComponent extends StatelessWidget {
  const HeaderComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 25, horizontal: 20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text(
                    'Hey, Sokleng !',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.0,
                        fontSize: 20),
                  ),
                  Text('Today',
                      style:
                          TextStyle(fontWeight: FontWeight.w200, fontSize: 15))
                ],
              ),
              Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage('lib/assets/images/profile.png'),
                    ),
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
