import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracky/screen/sign_in_screen.dart';

import 'home_screen.dart';

class StartingScreen extends StatelessWidget {
  const StartingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color.fromRGBO(159, 161, 223, 25), Color(0xff7970C5)],
          ),
        ),
        child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    height: 180,
                    child:
                        Image.asset('lib/assets/images/starting_screen.png')),
                const SizedBox(
                  height: 30,
                ),
                const Text("Hi there,",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w400)),
                const SizedBox(
                  height: 10,
                ),
                const Text("I’m Tracky",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.w400)),
                const SizedBox(
                  height: 30,
                ),
                const Text("Your New Personal Accompany Partner",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w100)),
                const SizedBox(
                  height: 130,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                      shadowColor: Colors.black,
                      fixedSize: const Size(350, 55),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50))),
                  onPressed: () {
                    Get.to(const AuthenticationScreen());
                  },
                  child: const Text("HI, TRACKY!",
                      style: TextStyle(color: Color(0xffCCCCCC), fontSize: 20)),
                )
              ],
            ))));
  }
}
