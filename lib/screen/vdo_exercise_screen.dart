import 'package:flutter/material.dart';
import 'package:tracky/components/header.dart';

class VdoExerciseScreen extends StatefulWidget {
  const VdoExerciseScreen({ Key? key }) : super(key: key);

  @override
  _VdoExerciseScreenState createState() => _VdoExerciseScreenState();
}

class _VdoExerciseScreenState extends State<VdoExerciseScreen> {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      body: Column(children:  [
        const HeaderComponent(),
        const Text('My Exercise'),
        Container(
          
          child: Column(
            children: [
              Text('WEEK 1'),
              Text('Workout 1 of 5'),

              Container(
                child: Row(
                  
                ),
              ),
            ],
          )
        )
      ],),
    );
  }
}