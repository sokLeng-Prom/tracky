import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tracky/rs/constants.dart';

import 'home_screen.dart';

class AuthenticationScreen extends StatefulWidget {
  const AuthenticationScreen({Key? key}) : super(key: key);

  @override
  _AuthenticationScreenState createState() => _AuthenticationScreenState();
}


class _AuthenticationScreenState extends State<AuthenticationScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isLoading = false;
  bool passwordVisible = false;

  @override 
  void initState() {
  
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          Stack(children: [
            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40)),
                color: ConstantColors.secondaryColor,
              ),
              height: MediaQuery.of(context).size.height * 0.5,
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.18,
              left: MediaQuery.of(context).size.width * 0.43,
              child: Container(
                width: 60,
                height: 60,
                // color: Colors.white,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(15))),
              ),
            ),
            Positioned(
                top: MediaQuery.of(context).size.height * 0.20,
                left: MediaQuery.of(context).size.width * 0.455,
                child: const Icon(
                  FontAwesomeIcons.heartbeat,
                  size: 40.0,
                  color: ConstantColors.secondaryColor,
                )),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.28,
              left: MediaQuery.of(context).size.width * 0.39,
              child: const Text('TRACKY',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 26,
                      fontWeight: FontWeight.bold)),
            )
          ]),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 60, vertical: 40),
            child: Column(
              children: [
                const Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Welcome Back!",
                    style: TextStyle(
                        color: ConstantColors.secondaryColor, fontSize: 24),
                  ),
                ),
                const Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.only(top: 15),
                    child: Text(
                      "Email",
                      style: TextStyle(
                          color: ConstantColors.secondaryColor, fontSize: 13),
                    ),
                  ),
                ),
                TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    hintText: 'example@gmail.com',
                    contentPadding: EdgeInsets.all(0),
                  ),
                ),
                const Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: EdgeInsets.only(top: 15),
                    child: Text(
                      "Password",
                      style: TextStyle(
                          color: ConstantColors.secondaryColor, fontSize: 13),
                    ),
                  ),
                ),
                TextFormField(
                  controller: passwordController,
                  obscureText: !passwordVisible,
                  decoration: InputDecoration(
                    labelText: '********',
                    contentPadding: EdgeInsets.all(0),
                     suffixIcon: IconButton(
                      onPressed: (){
                        setState(() {
                          passwordVisible = !passwordVisible;
                        });
                      }, 
                      icon: Icon(
                      passwordVisible ? Icons.visibility : Icons.visibility_off, color: ConstantColors.secondaryColor,),
                    )
                  ),

                ),
              ],
            ),
          ),
          isLoading
              ? CircularProgressIndicator()
              : Container(
                  width: MediaQuery.of(context).size.width * 0.6,
                  height: 40,
                  child: TextButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              ConstantColors.secondaryColor),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: const BorderSide(
                                          color:
                                              ConstantColors.secondaryColor)))),
                        onPressed: (){
                          Get.to(const HomeScreen());
                        },
                      child: const Text(
                        "login",
                        style: TextStyle(color: Colors.white),
                      )),
                ),
        ],
      ),
    ));
  }
}
