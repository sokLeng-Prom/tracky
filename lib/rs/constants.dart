import 'package:flutter/material.dart';

class ConstantColors {
  static const Color primaryColor = Color(0xff9FA1DF);

  static const Color backgroundColor = Color(0xffF2F6FF);

  static const Color secondaryColor = Color(0xff8083DD);
}