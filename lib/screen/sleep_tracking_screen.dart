import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:tracky/components/header.dart';

class SleepTrackingScreen extends StatefulWidget {
  const SleepTrackingScreen({ Key? key }) : super(key: key);

  @override
  _SleepTrackingScreenState createState() => _SleepTrackingScreenState();
}

class _SleepTrackingScreenState extends State<SleepTrackingScreen> {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        items: const <BottomNavigationBarItem>[
          
          BottomNavigationBarItem(icon: Icon(Icons.home,color: Colors.black,size: 30,), label: '',),
          BottomNavigationBarItem(icon: Icon(Icons.history,size: 30), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.person,size: 30), label: ''),
        ],
      ),
      backgroundColor: const Color(0xffFFFFFF),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const HeaderComponent(),
          Stack(
            children: [
          Container(
            margin: const EdgeInsets.only(left: 30),
            child: Lottie.network(
                'https://assets9.lottiefiles.com/private_files/lf30_em0dkba6.json',
                height: 350,
                alignment: Alignment.center),
          ),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.2,
              left: MediaQuery.of(context).size.width * 0.41,
              child: Column(
                children: const [
                  Text('90%',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 25)),
                  Text('7HOURS', style: TextStyle(fontSize: 20)),
                ],
              ))
            ],
          ),
          Container(
              padding: const EdgeInsets.only(top: 20),
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: const Text('You get enough sleep!',
                  style: TextStyle(
                      letterSpacing: 1.0,
                      fontWeight: FontWeight.w500,
                      fontSize: 20))),
          Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: const Text('Goal: 8HOURS',
                  style: TextStyle(
                      letterSpacing: 1.0,
                      fontWeight: FontWeight.w300,
                      fontSize: 15))),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: const [
                BoxShadow(
                  // color: Colors.black,
                  blurRadius: 0.7,
                  offset: Offset(0, 1),
                )
              ],
            ),
            height: 100,
            width: MediaQuery.of(context).size.width * 0.9,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Image.asset('lib/assets/images/water-drop.png'),
                const SizedBox(
                  width: 20,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text('Input Sleeping Hours',
                        style: TextStyle(
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.w500,
                            fontSize: 13)),
                    Text('7HOUR',
                        style: TextStyle(
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.w500,
                            fontSize: 20)),
                  ],
                ),
                const SizedBox(
                  width: 60,
                ),
                Container(
                    height: 20,
                    width: 65,
                    decoration: BoxDecoration(
                      color: const Color(0xffb9fee7),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: const Text(
                      'Normal',
                      textAlign: TextAlign.center,
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}