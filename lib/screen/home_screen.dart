import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracky/rs/constants.dart';
import 'package:tracky/screen/sleep_tracking_screen.dart';
import 'package:tracky/screen/vdo_exercise_screen.dart';
import 'package:tracky/screen/water_track_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        // backgroundColor: ConstantColors.backgroundColor,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home,color: Colors.black,size: 30,), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.history,size: 30), label: ''),
          BottomNavigationBarItem(icon: Icon(Icons.person,size: 30), label: ''),
        ],
      ),
      backgroundColor: ConstantColors.backgroundColor,
      body: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Image.asset('lib/assets/images/profile.png'),
              const SizedBox(
                width: 20,
              ),
              //dynamic
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text('Sokleng',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 29,
                      color: const Color(0xff000000).withOpacity(0.8),
                    )),
              )
            ],
          ),

          const Padding(padding: EdgeInsets.symmetric(vertical: 15.0)),
          // statistic
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.95,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: const [
                    //dynamic
                    Text('80%',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold)),
                    Text('sleeping'),
                  ],
                ),
                Column(
                  children: const [
                    //dynamic
                    Text('95%',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold)),
                    Text('water'),
                  ],
                ),
                Column(
                  children: const [
                    //dynamic
                    Text('90%',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold)),
                    Text('exercise'),
                  ],
                ),
              ],
            ),
          ),

          const Padding(padding: EdgeInsets.symmetric(vertical: 15.0)),

          GestureDetector(
            onTap: (){
                Get.to(const SleepTrackingScreen());
              },
            child: Container(
              
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    // color: Colors.black,
                    blurRadius: 0.5,
                    offset: Offset(0, 1),
                  )
                ],
              ),
              height: 80,
              width: MediaQuery.of(context).size.width * 0.8,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const Text('SLEEPING',style: TextStyle(fontWeight: FontWeight.bold),),
                  Image.asset('lib/assets/images/sleep.png')
                ],
              ),
            ),
          ),
          const SizedBox(height: 20),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.8,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: (){
                          Get.to(const WaterTrackingScreen());
                        },
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: ConstantColors.primaryColor,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: const [
                            BoxShadow(
                              blurRadius: 0.5,
                              offset: Offset(0, 1),
                            )
                          ],
                        ),
                        height: 180,
                        width: MediaQuery.of(context).size.width * 0.37,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              children: [
                                const SizedBox(
                                  height: 20,
                                ),
                                Image.asset('lib/assets/images/water-drop.png'),
                              ],
                            ),
                            Column(
                              children: const [
                                SizedBox(
                                  height: 140,
                                ),
                                Text('WATER',
                                    style: TextStyle(
                                      color: Colors.white,
                                    )),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: (){
                          Get.to(const VdoExerciseScreen());
                        },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: const [
                            BoxShadow(
                              blurRadius: 0.5,
                              offset: Offset(0, 1),
                            )
                          ],
                        ),
                        height: 180,
                        width: MediaQuery.of(context).size.width * 0.37,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              children: [
                                const SizedBox(
                                  height: 20,
                                ),
                                Image.asset('lib/assets/images/exercise.png'),
                              ],
                            ),
                            Column(
                              children: const [
                                SizedBox(
                                  height: 140,
                                ),
                                Text('EXERCISE',
                                    style: TextStyle(
                                      color: Colors.black,
                                    )),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}


// https://codewithandrea.com/articles/multiple-navigators-bottom-navigation-bar/